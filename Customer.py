#global func
def TampilInfo(IdNumber, Nama):
    return print("Id : ", IdNumber, "\nNama : ", Nama)

#base class
class Customer:
    '''get set'''
    def __init__(self, IdNumber, Nama, Alamat, Telepon):
        self.IdNumber = IdNumber
        self.Nama = Nama
        self.Alamat = Alamat
        self.Telepon = Telepon
        
#subclass Member
class Member(Customer):
    '''get set'''
    def __init__(self, IdNumber, Nama, Alamat, Telepon, HargaMember, Diskon):
        Customer.__init__(self, IdNumber, Nama, Alamat, Telepon)
        self.HargaMember = HargaMember
        self.Diskon = Diskon
        self.IdNumber = IdNumber

    def TampilBiaya(self):
        return print("Biaya : ",self.HargaMember - (self.HargaMember * self.Diskon / 100), "\n")
    
    def Info(self):
        return TampilInfo(self.IdNumber, self.Nama)
    
#subclass 
class NonMember(Customer):
    '''get set'''
    def __init__(self, IdNumber, Nama, Alamat, Telepon, HargaNonMember):
        Customer.__init__(self, IdNumber, Nama, Alamat, Telepon)
        self.HargaNonMember = HargaNonMember
        self.IdNumber

    def TampilBiaya(self):
        return print("Biaya : ",self.HargaNonMember)

    def Info(self):
        return TampilInfo(self.IdNumber, self.Nama)
    
Customer1 = Member("111", "Wahiddin Ishak", "Jakarta", "09809809", 500, 10)
Customer2 = NonMember("222", "Rona Pelangi", "Jakarta", "09809809", 300)
CustomerList = [Customer1, Customer2]
for Data in CustomerList:
    Data.Info()
    Data.TampilBiaya()
