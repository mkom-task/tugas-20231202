#global func
def TampilInfo(IdNumber, Nama):
    return print("Id : ", IdNumber, "\nNama : ", Nama)

#base class Pegawai
class Pegawai:
    '''get set'''
    def __init__(self, IdNumber, Nama, Telepon, Department):
        self.IdNumber = IdNumber
        self.Nama = Nama
        self.Telepon = Telepon
        self.Department = Department
    
#subclass PegawaiTetap
class PegawaiTetap(Pegawai):
    '''get set'''
    def __init__(self, IdNumber, Nama, Telepon, Department, GajiPokok, UangMakan, UangTransport):
        Pegawai.__init__(self, IdNumber, Nama, Telepon, Department)
        self.GajiPokok = GajiPokok
        self.UangMakan = UangMakan
        self.UangTransport = UangTransport
        self.IdNumber = IdNumber

    def TampilGaji(self):
        return print("Gaji : ", self.GajiPokok + self.UangMakan + self.UangTransport, "\n")
    
    def Info(self):
        return TampilInfo(self.IdNumber,self.Nama)
    
#subclass PegawaiHonor
class PegawaiHonor(Pegawai):
    '''get set'''
    def __init__(self, IdNumber, Nama, Telepon, Department, Honor):
        Pegawai.__init__(self, IdNumber, Nama, Telepon, Department)
        self.Honor = Honor
        self.IdNumber = IdNumber
    
    def TampilGaji(self):
        return print("Gaji : ", self.Honor, "\n")
    
    def Info(self):
        return TampilInfo(self.IdNumber, self.Nama)
    
Pegawai1 = PegawaiTetap("111","Wahiddin Ishak", "091231231233", "IT", 500, 250, 100)
Pegawai2 = PegawaiHonor("222","Rona Pelangi", "091231231444", "HR", 200)

PegawaiList = [Pegawai1, Pegawai2]
for Data in PegawaiList:
    Data.Info()
    Data.TampilGaji()
